import os
import sys
import platform

OS = platform.system()
BLOCK = u'\u2588'


def clear_screen():
    os.system('cls' if OS == 'Windows' else 'clear')


def print_cmd_qr(qrText, white=BLOCK, black='  ', enableCmdQR=True):
    blockCount = int(enableCmdQR)
    if abs(blockCount) == 0:
        blockCount = 1
    white *= abs(blockCount)
    if blockCount < 0:
        white, black = black, white
    sys.stdout.write(' ' * 50 + '\r')
    sys.stdout.flush()
    qr = qrText.replace('0', white).replace('1', black)
    sys.stdout.write(qr)
    sys.stdout.flush()


def print_qr(file):
    os.startfile(file)


QR_IMAGE_PATH = os.path.join("data, "'qr.png')
DETAIL_DATA_PATH = os.path.join("data", "detail_data.json")
RESULT_DATA_PATH = os.path.join("data", "result_data.json")
COOKIES_FILE_PATH = os.path.join("data", "cookies.txt")

# 如果data目录不存在, 则创建目录
if not os.path.exists('data'):
    os.mkdir('data')
